extends LinkButton

export(String) var scene_to_load

func _on_New_Game_pressed():
	global.reset_flag = true
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
