extends Node2D

var buff_flag_pos = global.buff_flag_pos
var buff_flag_visible = global.buff_icon_visible
var cur_buff = global.current_buff
func _ready():
	$CanvasLayer.visible_changer("1",global.buff_icon_visible)
	$CanvasLayer.visible_changer("2",global.buff_icon_visible)
	$CanvasLayer.visible_changer("3",global.buff_icon_visible)
	buff_flag_pos = 0
	
func _process(delta):
	if global.buff_icon_visible == true:
		if global.buff_flag_pos == 1:
			#$CanvasLayer.changebuff1(global.current_buff)
			$CanvasLayer.change_buff(global.current_buff, "buff1")
			$CanvasLayer.visible_changer("1",global.buff_icon_visible)
			
		elif global.buff_flag_pos == 2:
			#$CanvasLayer.changebuff2(global.current_buff)
			$CanvasLayer.change_buff(global.current_buff, "buff2")
			$CanvasLayer.visible_changer("2",global.buff_icon_visible)
			
		elif global.buff_flag_pos == 3:
			#$CanvasLayer.changebuff3(global.current_buff)
			$CanvasLayer.change_buff(global.current_buff, "buff3")
			$CanvasLayer.visible_changer("3",global.buff_icon_visible)
			
		buff_flag_visible = false
	if global.shoot_reset == true:
		$CanvasLayer.visible_changer("1",false)
		$CanvasLayer.visible_changer("2",false)
		$CanvasLayer.visible_changer("3",false)
		global.shoot_reset = false
	
