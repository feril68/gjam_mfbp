extends KinematicBody2D

var is_dead = false
export (String) var sceneName = "GameOver"
export var health = 3
export var reset_health = 2
export var state = "green"

var frog_green = preload("res://Assets/Fro_Green.png")
var frog_red = preload("res://Assets/Fro_red.png")
var frog_orange = preload("res://Assets/Fro_Orange.png")

func _ready():
	if state == "green":
		$Sprite.set_texture(frog_green)
	elif state == "red":
		$Sprite.set_texture(frog_red)
	elif state == "orange":
		$Sprite.set_texture(frog_orange)

func _process(delta):
	$Label.text = str(health)
	if is_dead == true:
		get_node("CollisionFrog").disabled = true
		get_node("Area2D/CollisionShape2D").disabled = true
func dead():
	if state == "green":
		health = reset_health
		is_dead = false
		state = "red"
		$Sprite.set_texture(frog_red)
	elif state == "red":
		health = reset_health
		is_dead = false
		state = "orange"
		$Sprite.set_texture(frog_orange)
	else:
		is_dead = true
		$Sprite.visible = false
		queue_free()
	
func state_changer(state_name):
	state = state_name
	
func get_health():
	return health

func health_changer(minus_health):
	health = health - minus_health

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		global.buff_icon_visible = false
		global.buff_flag_pos = 0
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
