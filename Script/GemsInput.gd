extends Node2D

var purple = preload("res://Assets/orbx/gem04purple.png")
var yellow = preload("res://Assets/orbx/gem03yellow.png")

func changeOrb(gem, color):
	get_node(gem).visible = true
	if color == "purple":
		get_node(gem).set_texture(purple)
	elif color == "yellow":
		get_node(gem).set_texture(yellow)

func cast_active():
	visibles_off()
	$cast_active.visible = true
	get_node("cast_active/AnimationPlayer").play("burn")

func cast_off():
	$cast_active.visible = false
	
func visibles_off():
	$gem1.visible = false
	$gem2.visible = false