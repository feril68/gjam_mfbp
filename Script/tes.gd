extends Node2D

var rock = preload("res://Assets/orbx/gem01orange.png")
var buff = preload("res://Assets/orbx/gem02blue.png")
var buff_sepatu = preload("res://Assets/orbx/gem04purple.png")

func changeOrb(orb:String):
	if orb == "rock":
		get_node("Sprite").set_texture(rock)
	elif orb == "buff":
		get_node("Sprite").set_texture(buff)
	elif orb == "buff_sepatu":
		get_node("Sprite").set_texture(buff_sepatu)