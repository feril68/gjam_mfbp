extends CanvasLayer

var mov_spd = preload("res://Assets/rip_buff_icon/mov_speed.PNG")
var shoot_spd = preload("res://Assets/rip_buff_icon/shoot_speed.PNG")
var no_wall = preload("res://Assets/rip_buff_icon/no_wall.PNG")
var jump_spd = preload("res://Assets/rip_buff_icon/jump_speed.PNG")
var shoot_dmg = preload("res://Assets/rip_buff_icon/shoot_dmg.PNG")

func change_buff(buff_name, node_name):
	if buff_name == "mov_spd":
		get_node(node_name).set_texture(mov_spd)
	elif buff_name == "shoot_spd":
		get_node(node_name).set_texture(shoot_spd)
	elif buff_name == "no_wall":
		get_node(node_name).set_texture(no_wall)
	elif buff_name == "jump_spd":
		get_node(node_name).set_texture(jump_spd)
	elif buff_name == "shoot_dmg":
		get_node(node_name).set_texture(shoot_dmg)
		
func visible_changer(number, boolean):
	if number == "1":
		$buff1.visible = boolean
	if number == "2":
		$buff2.visible = boolean
	if number == "3":
		$buff3.visible = boolean