extends Sprite

var rock = preload("res://Assets/PIPOYA WARP 192x192/pipo-gate01a192.png")
var buff = preload("res://Assets/PIPOYA WARP 192x192/pipo-gate01b192.png")
var buff_sepatu = preload("res://Assets/PIPOYA WARP 192x192/pipo-gate01c192.png")

func changeOrb(orb:String):
	if orb == "rock":
		$".".set_texture(rock)
	elif orb == "buff":
		$".".set_texture(buff)
	elif orb == "buff_sepatu":
		$".".set_texture(buff_sepatu)
