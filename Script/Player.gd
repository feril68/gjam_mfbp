extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const FIXED_SPEED = 200
const FIXED_JUMP = -400

const UP = Vector2(0,-1)
var velocity = Vector2()
onready var timer = get_node("CastTimer")

var health = 100
var flag_rock = false
var flag_buff = false
var flag_buff_ready = false
var flag_buff_movement = false
var flag_no_input = false
var arr_combo : Array = []
var combo_counter = 0
var flag_buff_ada = false
var array_buff_icon : Array = []
var arr_combo_counter = 0

var flag_spell_ready = false


const FIREBALL = preload("res://Scenes/fireball.tscn")

func _ready():
	timer.set_wait_time(0.3)
	$Sprite.play("idle")
	$GemsInput.visibles_off()
	set_process_input(true)

func get_input():
	velocity.x = 0
		
	if Input.is_action_pressed('right') :
		$Sprite.flip_h = false
		$Sprite.play("walk")
		velocity.x += speed
		if sign($Position2D.position.x) == -1:
			$Position2D.position.x *= -1
			$Orb.position.x *= -1
		
	if Input.is_action_pressed('left') :
		$Sprite.flip_h = true
		$Sprite.play("walk")
		velocity.x -= speed
		if sign($Position2D.position.x) == 1:
			$Position2D.position.x *= -1
			$Orb.position.x *= -1
	
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
			
	if Input.is_action_just_released("left") or Input.is_action_just_released("right"):
		$Sprite.play("idle")
		
	if Input.is_action_just_pressed("shoot") and flag_spell_ready == true:
		if flag_buff_ready == true:
			flag_buff_ready = false
			print("ready")
		var fireball = FIREBALL.instance()
		if sign($Position2D.position.x) == 1:
			fireball.set_fireball_direction(1)
		else:
			fireball.set_fireball_direction(-1)
		get_parent().add_child(fireball)
		fireball.position = $Position2D.global_position
		global.fireball_speed = global.FIREBALL_SPEED
		flag_spell_ready = false
		
		speed = 200
		jump_speed = -400
		flag_buff_ada = false
		
		global.shoot_reset = true
		global.buff_flag_pos = 0
		array_buff_icon.clear()
	
	if (flag_rock == true or flag_buff == true or flag_buff_movement == true) and flag_no_input == false:
		var flag_color = "null"
		if combo_counter != 2:
			if Input.is_action_just_pressed("D"):
				flag_color = "purple"
				combo_counter = combo_counter + 1
				arr_combo.append("D")

			elif Input.is_action_just_pressed("S"):
				flag_color = "yellow"
				combo_counter = combo_counter + 1
				arr_combo.append("S")

			if flag_color != "null":
				if arr_combo.size() == 1:
					$GemsInput.changeOrb("gem1", flag_color)
				elif arr_combo.size() == 2:
					$GemsInput.changeOrb("gem2", flag_color)
		elif combo_counter == 2:
			flag_color = "null"
			flag_no_input = true
			timer.start()
			
			var combo1 = arr_combo[0]
			var combo2 = arr_combo[1]
			var counter_on = false
			if flag_buff == true:
				if combo1 == "S" and combo2 == "S":
					global.fireball_speed = global.fireball_speed + 200
					global.current_buff = "shoot_spd"
					if flag_buff_ada == false and global.buff_flag_pos + 1 != 4:
						array_buff_icon.append("shoot_spd")
					counter_on = true
				elif combo1 == "D" and combo2 == "D":
					global.fireball_damage += 1
					global.current_buff = "shoot_dmg"
					if flag_buff_ada == false and global.buff_flag_pos + 1 != 4:
						array_buff_icon.append("shoot_dmg")
					counter_on = true
					
			elif flag_buff_movement == true:
				if combo1 == "S" and combo2 == "S":
					speed += 100
					global.current_buff = "mov_spd"
					if flag_buff_ada == false and global.buff_flag_pos + 1 != 4:
						array_buff_icon.append("mov_spd")
					counter_on = true
				elif combo1 == "S" and combo2 == "D":
					jump_speed -= 100
					global.current_buff = "jump_spd"
					if flag_buff_ada == false and global.buff_flag_pos + 1 != 4:
						array_buff_icon.append("jump_spd")
					counter_on = true
					
			elif flag_rock == true:
				if combo1 == "S" and combo2 == "S":
					global.flag_collision = false
					global.current_buff = "no_wall"
					
					global.wall_counter += 1
					
					if flag_buff_ada == false and global.buff_flag_pos + 1 != 4:
						array_buff_icon.append("no_wall")
					counter_on = true
					
			flag_rock = false
			flag_buff = false
			flag_buff_movement = false
			
			flag_rock = false
			arr_combo.clear()
			combo_counter = 0
			flag_spell_ready = true
			$Orb.visible = false
			
			if counter_on == true:
				if global.buff_flag_pos + 1 == 4:
					
					global.buff_flag_pos = 1
					flag_buff_ada = true
					if flag_buff_ada == true:
						buff_changer(array_buff_icon[0])
						array_buff_icon[0] = global.current_buff
				else:
					global.buff_flag_pos += 1
					if flag_buff_ada == true:
						buff_changer(array_buff_icon[global.buff_flag_pos-1])
						array_buff_icon[global.buff_flag_pos-1] = global.current_buff
				global.buff_icon_visible = true
				counter_on = false
			
	if Input.is_action_just_pressed("ROCK") and flag_no_input == false:
		$GemsInput.visibles_off()
		flag_rock = true
		flag_buff = false
		flag_buff_movement = false
		arr_combo.clear()
		combo_counter = 0
		$Orb.visible = true
		$Orb.changeOrb("rock")
		
	if Input.is_action_just_pressed("buff") and flag_no_input == false:
		$GemsInput.visibles_off()
		flag_buff = true
		flag_rock = false
		flag_buff_movement = false
		arr_combo.clear()
		combo_counter = 0
		$Orb.visible = true
		$Orb.changeOrb("buff")
		
	if Input.is_action_just_pressed("buff_speed") and flag_no_input == false:
		$GemsInput.visibles_off()
		flag_buff = false
		flag_rock = false
		flag_buff_movement = true
		arr_combo.clear()
		combo_counter = 0
		$Orb.visible = true
		$Orb.changeOrb("buff_sepatu")
		
		

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func buff_changer(buff_name):
	if buff_name == "shoot_spd":
		global.fireball_speed = global.fireball_speed - 200
	elif buff_name == "mov_spd":
		speed -= 100
	elif buff_name == "no_wall":
		global.flag_collision = true
		global.wall_counter -= 1
	elif buff_name == "jump_spd":
		jump_speed += 100
	elif buff_name == "shoot_dmg":
		global.fireball_damage -= 1

func _on_CastTimer_timeout():
	timer.stop()
	$GemsInput.cast_active()
	flag_no_input = false
