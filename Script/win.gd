extends Area2D

export (String) var sceneName = "WinScene"



func _on_WinArea_body_entered(body):
	if body.get_name() == "Player":
		global.buff_icon_visible = false
		global.buff_flag_pos = 0
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
