extends Area2D

var fireball_speed = global.fireball_speed
var fireball_damage = global.fireball_damage
var velocity = Vector2()
var direction = 1

const EXPLOSION = preload("res://Scenes/explosion.tscn")

func _ready():
	pass 

func set_fireball_direction(dir):
	direction = dir
	if dir == -1:
		$Sprite.flip_h = true

func _physics_process(delta):
	velocity.x = fireball_speed * delta * direction
	translate(velocity)

func set_fireball_speed(new_speed):
	fireball_speed = new_speed

func _on_VisibilityNotifier2D_screen_exited():
	global.fireball_damage = 1
	queue_free()


func _on_fireball_body_entered(body):
	if body.get_name() == "Player":
		pass
		
	elif body.get_name() == "EnemyFrog" or body.get_name() == "EnemyFrog2" or body.get_name() == "EnemyFrog3" or body.get_name() == "EnemyFrog4" or body.get_name() == "EnemyFrog5" and global.flag_collision == false:
		global.flag_collision = true
		global.wall_counter = 0
		if body.health - global.fireball_damage > 0:
			body.health -= global.fireball_damage
		elif body.health -  global.fireball_damage <= 0 :
			queue_free()
			body.dead()
		global.fireball_damage = 1
		
	elif body.get_name() == "EnemyFrog" or body.get_name() == "EnemyFrog2" or body.get_name() == "EnemyFrog3" or body.get_name() == "EnemyFrog4" or body.get_name() == "EnemyFrog5":
		global.flag_collision = true
		global.wall_counter = 0
		if body.health - global.fireball_damage > 0:
			body.health -= global.fireball_damage
		elif body.health -  global.fireball_damage <= 0 :
			queue_free()
			body.dead()
		global.fireball_damage = 1

		
	if body.get_name() == "TileMap" and global.flag_collision == false:
		if global.wall_counter != 0:
			global.wall_counter -= 1
		if global.wall_counter == 0:
			global.flag_collision = true
	
	
		
	
	else:
		create_explosion()
		queue_free()
		
func create_explosion():
	var explosion = EXPLOSION.instance()
	explosion.position = get_global_position()
	get_parent().add_child(explosion)
	
